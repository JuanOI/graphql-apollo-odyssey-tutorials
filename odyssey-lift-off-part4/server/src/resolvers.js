import * as fs from "node:fs";
import { PubSub } from "graphql-subscriptions";

const pubsub = new PubSub();

const resolvers = {
  Query: {
    // returns an array of Tracks that will be used to populate the homepage grid of our web client
    tracksForHome: (_, __, { dataSources }) => {
      return dataSources.trackAPI.getTracksForHome();
    },

    // get a single track by ID, for the track page
    track: (_, { id }, { dataSources }) => {
      return dataSources.trackAPI.getTrack(id);
    },

    // get a single module by ID, for the module detail page
    module: (_, { id }, { dataSources }) => {
      return dataSources.trackAPI.getModule(id);
    },

    // get file name
    file: () => {
      var files = fs.readdirSync(
        "/Users/juan/src/sandbox/graphql-apollo-odyssey-tutorials/odyssey-lift-off-part4/server/mock/"
      );
      console.log(files[0]);
      return files[0];
    },
  },
  Mutation: {
    // increments a track's numberOfViews property
    incrementTrackViews: async (_, { id }, { dataSources }) => {
      try {
        const track = await dataSources.trackAPI.incrementTrackViews(id);
        return {
          code: 200,
          success: true,
          message: `Successfully incremented number of views for track ${id}`,
          track,
        };
      } catch (err) {
        return {
          code: err.extensions.response.status,
          success: false,
          message: err.extensions.response.body,
          track: null,
        };
      }
    },
    changeFileName: () => {
      fs.renameSync(
        "/Users/juan/src/sandbox/graphql-apollo-odyssey-tutorials/odyssey-lift-off-part4/server/mock/file.txt",
        "/Users/juan/src/sandbox/graphql-apollo-odyssey-tutorials/odyssey-lift-off-part4/server/mock/file_new.txt",
        () => {
          console.log("Renamed file!");
        }
      );
      pubsub.publish("FILE_NAME_CHANGED", {
        fileNameChanged: {
          name: "file_new.txt",
        },
      });
      return {
        code: 200,
        success: true,
        message: "Successfully renamed file!",
        name: "file_new.txt",
      };
    },
  },
  Subscription: {
    fileNameChanged: {
      subscribe: () => pubsub.asyncIterator(["FILE_NAME_CHANGED"]),
    },
  },
  Track: {
    author: ({ authorId }, _, { dataSources }) => {
      return dataSources.trackAPI.getAuthor(authorId);
    },

    modules: ({ id }, _, { dataSources }) => {
      return dataSources.trackAPI.getTrackModules(id);
    },
  },
  File: {
    name: () => {
      var files = fs.readdirSync(
        "/Users/juan/src/sandbox/graphql-apollo-odyssey-tutorials/odyssey-lift-off-part4/server/mock/"
      );
      console.log(files[0]);
      return files[0];
    },
  },
};

export default resolvers;
