import React from "react";
import styled from "@emotion/styled";
import { colors } from "../styles";
import { useQuery, gql } from "@apollo/client";
import { Layout, QueryResult } from "../components";

const FILE_NAME_CHANGED_SUBSCRIPTION = gql`
  subscription OnFileNameChanged {
    fileNameChanged {
      file
    }
  }
`;

/** GET_TRACK gql query to retrieve a specific track by its ID */
const GET_FILE = gql`
  query getFile {
    file
  }
`;

/**
 * Track Page fetches a track's data from the gql query GET_TRACK
 * and provides it to the TrackDetail component to display
 */
const File = () => {
  const { subscribeToMore, ...result } = useQuery(GET_FILE);

  console.log(result.data);

  // RETURN NEEDS FINISHING - QueryResult MIGHT ALSO NEED UPDATING!!!!
  return (
    <Layout>
      <QueryResult
        {...result}
        subscribeToNewFileName={() =>
          subscribeToMore({
            document: FILE_NAME_CHANGED_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
              if (!subscriptionData.data) return prev;
              const newFileName = subscriptionData.data.fileNameChanged;

              return newFileName;
            },
          })
        }
      >
        <DetailRow>
          <h1>{result.data?.file}</h1>
        </DetailRow>
      </QueryResult>
    </Layout>
  );
};

export default File;

const DetailRow = styled.div({
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  width: "100%",
  paddingBottom: 20,
  marginBottom: 20,
  borderBottom: `solid 1px ${colors.silver.dark}`,
});
